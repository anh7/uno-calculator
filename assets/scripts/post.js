﻿var Post = (function() {
    function handleSavePost() {
        var id = $('.input-post-id').val();
        var title = $('.input-post-title').val();
        var alias = $('.input-post-alias').val();
        var date = Util.toJSONDate($('.input-post-date').val(),$('.input-post-time').val());
        var content = $('.input-post-content').html();
        var data = {
            id: id,
            title: title,
            date: date,
            content: content
        }
        $.ajax({
            url: '/post/save',
            type: 'post',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.result === 1) {
                    toastr.success('Đã lưu thành công');
                } else
                    toastr.error('Có lỗi xảy ra');
            },
            error: function (message) {
                console.log(message);
                toastr.error('Có lỗi xảy ra');
            }
        });
    }
    function handleCreatePost() {
        var id = -1;
        var title = $('.input-post-title').val();
        var alias = $('.input-post-alias').val();
        var date = Util.toJSONDate($('.input-post-date').val(), $('.input-post-time').val());
        var content = $('.input-post-content').html();
        var data = {
            id: id,
            title: title,
            date: date,
            content: content,
            alias: alias
        }
        $.ajax({
            url: '/post/create',
            type: 'post',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.result > 0) {
                    toastr.success('Đã tạo thành công');
                    var submit = $('.input-post-submit-create')
                    submit.html('Qua trang chỉnh sửa');
                    submit.unbind('click', handleCreatePost);
                    submit.click(function () {
                        window.location.href = '/post/' + response.result
                    });
                    $('.input-post-id').val(response.result);
                    $('.input-delete').removeClass('hidden');
                } else
                    toastr.error('Có lỗi xảy ra');
            },
            error: function (message) {
                console.log(message);
                toastr.error('Có lỗi xảy ra');
            }
        });
    }

    function handleDelete() {
        var id = $('.input-post-id').val();
        if (id != null && id != undefined) {
            $.ajax({
                url: '/post/delete',
                type: 'post',
                data: id,
                dataType: "json",
                success: function (response) {
                    if (response.result === 1) {
                        window.location.href = '/post';
                    } else {
                        toastr.error('Có lỗi xảy ra');
                        console.log(response.message);
                    }
                },
                error: function (message) {
                    console.log(message);
                }
            });
        }
    }

    return {
        handleSavePost: handleSavePost,
        init: function () {
            $('.input-post-cancel').click(function() {
                window.location.href = '/post';
            });
            $('.btn-create-post').click(function () {
                window.location.href = '/post/create';
            });
            $('.input-post-submit').click(handleSavePost);
            $('.input-post-submit-create').click(handleCreatePost);
            $('.delete-modal-btn-delete').click(handleDelete);
            $("#delete-modal").draggable({
                handle: ".modal-header"
            });
        }
    }
}());

