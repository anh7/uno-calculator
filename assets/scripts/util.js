var Util = (function() {
    function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }

    function valueOfTimeString(str, separator) {
        if (str != null && str != undefined && separator != null && separator != undefined) {
            var parts = str.split(separator);
            var date = new Date(0);
            date.setHours(parts[0]);
            date.setMinutes(parts[1]);
            return date.getTime();
        }
    }

    function convertStringToDate(str, separator) {
        if (str != null && str != undefined && separator != null && separator != undefined) {
            var parts = str.split(separator);
            var date = new Date(parts[2], parts[1] - 1, parts[0]);
            //var offset = new Date().getTimezoneOffset();
            //return new Date(date.getTime() - offset*60000);
            return date;
        } else
            return new Date();
    }

    function toJSONDate(str, time) {
        var date = convertStringToDate(str, '/');
        var time = valueOfTimeString(time, ':');
        var offset = new Date().getTimezoneOffset() * 60000;
        var value = date.getTime() + time - offset;
        return '/Date(' + value + ')/';
    }
    
    function validateEmail(email) {
        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return reg.test(email);
    }

    return {
        updateQueryStringParameter: updateQueryStringParameter,
        convertStringToDate: convertStringToDate,
        toJSONDate: toJSONDate,
        valueOfTimeString: valueOfTimeString,
        validateEmail: validateEmail
    }
}());

