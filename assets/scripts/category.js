﻿var Category = ( function() {
    function handleCheckExists() {
        var elem = $(this);
        var data = elem.val();
        if (data.length == 0)
            return;
        var flag = elem.hasClass('input-category-name') ? 0 : 1;
        var id = $('.input-category-id').val();
        if (id.length == 0)
            id = -1;
        $.ajax({
            url: '/category/existed',
            type: 'post',
            data: JSON.stringify({
                id : id,
                data: data,
                flag: flag
            }),
            dataType: "json",
            success: function (response) {
                if (response.result === 1) {
                    if (flag == 1) {
                        var msg = 'Slug đã tồn tại';
                    } else {
                        var msg = 'Tên danh mục đã tồn tại';
                    }
                    toastr.error(msg);
                    elem.closest('.form-group').addClass('has-error').removeClass('has-success');
                } else {
                    if (flag == 1) {
                        var msg = 'Slug khả dụng';
                    } else {
                        var msg = 'Tên danh mục khả dụng';
                    }
                    toastr.success(msg);
                    elem.closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            },
            error: function (message) {
                console.log(message);
                return true;
            }
        });
        return false;
    }
    function handleSave() {
        var id = $('.input-category-id').val();
        var name = $('.input-category-name').val();
        var slug = $('.input-category-slug').val();
        var data = {
            id: id,
            name: name,
            slug: slug
        };
        $.ajax({
            url: '/category/save',
            type: 'post',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.result === 1) {
                    toastr.success('Đã lưu thành công');
                } else
                    toastr.error('Có lỗi xảy ra');
            },
            error: function (message) {
                console.log(message);
                toastr.error('Có lỗi xảy ra');
            }
        });
    }
    function handleCreate() {
        var id = -1;
        var name = $('.input-category-name').val();
        var slug = $('.input-category-slug').val();
        var data = {
            id: id,
            name: name,
            slug: slug
        }
        $.ajax({
            url: '/category/create',
            type: 'post',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.result > 0) {
                    toastr.success('Đã tạo thành công');
                    var submit = $('.input-post-submit-create');
                    submit.html('Qua trang chỉnh sửa');
                    submit.unbind('click', handleCreate);
                    submit.click(function () {
                        window.location.href = '/category/' + response.result
                    });
                } else
                    toastr.error('Có lỗi xảy ra');
            },
            error: function (message) {
                console.log(message);
                toastr.error('Có lỗi xảy ra');
            }
        });
    }

    function handleDelete() {
        var id = $('.input-category-id').val();
        if (id != null && id != undefined) {
            $.ajax({
                url: '/category/delete',
                type: 'post',
                data: id,
                dataType: "json",
                success: function (response) {
                    if (response.result === 1) {
                        window.location.href = '/category';
                    } else {
                        toastr.error('Có lỗi xảy ra');
                        console.log(response.message);
                    }
                },
                error: function (message) {
                    console.log(message);
                }
            });
        }
    }

    return {
        init: function() {
            $('.input-category').blur(handleCheckExists);
            $('.input-category-submit').click(handleSave);
            $('.input-category-submit-create').click(handleCreate);
            $('.input-category-cancel').click(function() {
                window.location.href = '/category';
            });
            $('.btn-create-category').click(function() {
                window.location.href = '/category/create';
            });
            $('.delete-modal-btn-delete').click(handleDelete);
            $("#delete-modal").draggable({
                handle: ".modal-header"
            });
        }
    };
}());