﻿var Account = (function () {
    function handleSaveAccount() {
        var passwordFlag = $('#password-flag').val();
        if (passwordFlag === '1') {
            var password = $('.input-account-password').val();
            var confirm = $('.input-account-password-confirm').val();
            if (password !== confirm) {
                toastr.error('Mật khẩu không giống nhau');
                return;
            }
        }
        var id = $('.input-account-id').val();
        var username = $('.input-account-username').val();
        var address = $('.input-account-address').val();
        var fullname = $('.input-account-fullname').val();
        var email = $('.input-account-email').val();
        var data = {
            id: id,
            username: username,
            password: password,
            email: email,
            fullname: fullname,
            address: address,
            savePassword: passwordFlag == '1' ? 1 : 0 
        }
        $.ajax({
            url: '/account/save',
            type: 'post',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.result === 1) {
                    toastr.success('Đã lưu thành công');
                } else
                    toastr.error('Có lỗi xảy ra');
            },
            error: function (message) {
                console.log(message);
                toastr.error('Có lỗi xảy ra');
            }
        });
    }

    function handleCreateAccount() {
        var passwordFlag = $('#password-flag').val();
        if (passwordFlag === '1') {
            var password = $('.input-account-password').val();
            var confirm = $('.input-account-password-confirm').val();
            if (password !== confirm) {
                toastr.error('Mật khẩu không giống nhau');
                return;
            }
        }
        if (!handleValidateEmail()) {
            return;
        }
        var id = $('.input-account-id').val();
        var username = $('.input-account-username').val();
        var address = $('.input-account-address').val();
        var fullname = $('.input-account-fullname').val();
        var email = $('.input-account-email').val();
        var data = {
            username: username,
            password: password,
            email: email,
            fullname: fullname,
            address: address,
        }
        $.ajax({
            url: '/account/create',
            type: 'post',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.result > 0) {
                    toastr.success('Đã tạo thành công');
                    var submit = $('.input-account-submit-create')
                    submit.html('Qua trang chỉnh sửa');
                    submit.unbind('click', handleCreateAccount);
                    submit.click(function() {
                        window.location.href = '/account/' + response.result;
                    });
                    $('.input-account-id').val(response.result);
                    $('.input-delete').removeClass('hidden');
                } else
                    toastr.error('Có lỗi xảy ra');
            },
            error: function (message) {
                console.log(message);
                toastr.error('Có lỗi xảy ra');
            }
        });
    }

    function handlePasswordChange() {
        var confirm = $(this).val();
        var password = $('.input-account-password').val();
        var changed = $(this);
        if (confirm !== password) {
            changed.closest('.form-group').addClass('has-error');
            toastr.error('Mật khẩu xác nhận không giống');
            changed.pulsate({
                color: "#bf1c56",
                repeat: false
            });
        } else {
            changed.closest('.form-group').removeClass('has-error').addClass('has-success');
        }
    }
    
    function handleChangeFlag() {
        $('#password-flag').val('1');
    }

    function checkUsernameExistence() {
        var elem = $(this);
        var username = elem.val();
        if (username != null && username != undefined) {
            $.ajax({
                url: '/account/existed',
                type: 'post',
                data: username,
                //contentType: "application/text; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.result === 1) {
                        toastr.error('Tên đăng nhập đã tồn tại');
                        elem.closest('.form-group').addClass('has-error').removeClass('has-success');
                        $('.input-account-submit, .input-account-submit-create').addClass('disabled');
                    } else {
                        toastr.success('Tên đăng nhập có thể sử dụng');
                        elem.closest('.form-group').removeClass('has-error').addClass('has-success');
                        $('.input-account-submit, .input-account-submit-create').removeClass('disabled');
                    }
                },
                error: function (message) {
                    console.log(message);
                    return true;
                }
            });
        }
        return false;
    }

    function handleValidateEmail() {
        var elem = $('.input-account-email');
        var email = elem.val();
        if (Util.validateEmail(email)) {
            elem.closest('.form-group').addClass('has-success').removeClass('has-error');
            $('.input-account-submit, .input-account-submit-create').removeClass('disabled');
            return true;
        } else {
            elem.closest('.form-group').removeClass('has-success').addClass('has-error');
            toastr.error('Email không hợp lệ')
            $('.input-account-submit, .input-account-submit-create').addClass('disabled');
            return false;
        }
    }

    function handleDelete() {
        var id = $('.input-account-id').val();
        if (id != null && id != undefined) {
            $.ajax({
                url: '/account/delete',
                type: 'post',
                data: id,
                dataType: "json",
                success: function (response) {
                    if (response.result === 1) {
                        window.location.href = '/account';
                    } else {
                        toastr.error('Có lỗi xảy ra');
                        console.log(response.message);
                    }
                },
                error: function (message) {
                    console.log(message);
                }
            });
        }
    }

    return {
        handleSaveAccount: handleSaveAccount,
        handleCreateAccount: handleCreateAccount,
        init: function () {
            $('.input-account-cancel').click(function () {
                window.location.href = '/account'
            });
            $('.btn-create-account').click(function () {
                window.location.href = '/account/create'
            });
            $('.input-password-check').change(handleChangeFlag);
            $('.input-account-email').blur(handleValidateEmail)
            $('.input-account-submit').click(handleSaveAccount);
            $('.input-account-password-confirm').blur(handlePasswordChange);
            $('.input-account-username').blur(checkUsernameExistence);
            $('.input-account-submit-create').click(handleCreateAccount);
            $('.delete-modal-btn-delete').click(handleDelete);
            $("#delete-modal").draggable({
                handle: ".modal-header"
            });
        }
    }
}());